import asyncio
import aiohttp
import pymongo
import os
import time
from bson.objectid import ObjectId
from Players import Player

from dotenv import load_dotenv

load_dotenv()


def get_players_list():
    players_db = col.find({})
    current_players_list = []
    for p in players_db:
        current_players_list.append(
            Player(
                p["_id"],
                p["username"],
                p["battletag"],
                p["race"],
                p["w3c_profile"],
                p["region"],
                p["s12_mmr"],
                p["s13_mmr"],
            )
        )

    return current_players_list


async def get_mmr(session: aiohttp.ClientSession, battletag):
    print(f"Fetching mmr for {battletag}")
    season = int(os.getenv("SEASON"))
    s12_mmr_list = [0]
    s13_mmr_list = [0]
    while season >= 12:
        print(f"Checking season {season} mmr for {battletag}")
        url = (
            os.getenv("W3C_URL")
            + battletag.replace("#", "%23")
            + "/game-mode-stats?gateWay=20&season="
            + str(season)
        )
        try:
            res = await session.request("GET", url=url)
            data = await res.json()
        except Exception as e:
            print(e)
            return {"battletag": battletag, "s13_mmr": 0, "s12_mmr": 0}

        for item in data:
            if item["gameMode"] == 1:
                if season == 12:
                    s12_mmr_list.append(item["mmr"])
                elif season == 13:
                    s13_mmr_list.append(item["mmr"])

        season -= 1

    return {
        "battletag": battletag,
        "s13_mmr": max(s13_mmr_list),
        "s12_mmr": max(s12_mmr_list),
    }


def update_mmr(players, mmr_list):
    for player in players:
        for mmr in mmr_list:
            if player.battletag == mmr["battletag"]:
                player.s13_mmr = mmr["s13_mmr"]
                player.s12_mmr = mmr["s12_mmr"]
            if not player.battletag:
                player.s13_mmr = 0
                player.s12_mmr = 0

    return players


async def update_player(player):
    w3c_profile_string = player["battletag"].replace("#", "%23")
    w3c_profile = f"https://w3champions.com/player/{w3c_profile_string}"
    print(
        f"Updating {player['battletag']} - \
            MMR: {player['s13_mmr']}"
    )
    col.update_one(
        {"_id": ObjectId(str(player["id"]))},
        {
            "$set": {
                "s13_mmr": player["s13_mmr"],
                "s12_mmr": player["s12_mmr"],
                "battletag_lower": player["battletag"].lower(),
                "w3c_profile": w3c_profile,
            }
        },
    )


async def main(players):
    async with aiohttp.ClientSession() as session:
        mmr_tasks = []
        for player in players:
            if player.battletag:
                time.sleep(0.05)
                mmr_tasks.append(get_mmr(session=session, battletag=player.battletag))

        mmr_list = await asyncio.gather(*mmr_tasks, return_exceptions=True)

        players = update_mmr(players[:], mmr_list)

    # update mmr in mongo for each player
    for player in players:
        if player.battletag != "":
            player = player.to_dict()
            await update_player(player)


def start():
    if os.name == "nt":
        # for windows
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(main(player_list))


client = pymongo.MongoClient(os.getenv("MONGO_URI"))
db = client[os.getenv("MONGO_DB")]
col = db[os.getenv("MONGO_COL")]
player_list = get_players_list()
