import asyncio
import aiohttp
import pymongo

from Players import Player
import os
from dotenv import load_dotenv

load_dotenv()


# get full list of player names from w3champions
async def get_w3c_list(session: aiohttp.ClientSession):
    season = os.getenv("SEASON")
    division = 0
    player_names = []
    while division < 46:  # currently 45 divisions
        url = f"https://website-backend.w3champions.com/api/ladder/{division}?gateWay=20&gameMode=1&season={season}"
        r = await session.request("GET", url=url)
        print("Getting division " + str(division))
        data = await r.json()
        if len(data) == 0:
            division += 1
            continue
        for player in data:
            player_battletag = player["playersInfo"][0]["battleTag"]
            player_names.append(player_battletag)
        division += 1

    return player_names


# get list of player objects from mongo
def get_mongo_list():
    players_db = col.find()
    current_players_list = []
    for p in players_db:
        current_players_list.append(
            Player(
                p["_id"],
                p["username"],
                p["battletag"],
                p["race"],
                p["w3c_profile"],
                p["region"],
                p["s12_mmr"],
                p["s13_mmr"],
            )
        )

    return current_players_list


def parse_profile_link(battletag):
    return battletag.replace("#", "%23")


# compare the two lists and update the mongo db with the values from w3champions
def update_names(w3c_names):
    print(w3c_names)
    mongo_list = get_mongo_list()
    for player in mongo_list:
        for p in w3c_names:
            if player.battletag.lower() == p.lower():
                print(f"Updating {player.battletag} to {p}")
                col.update_one(
                    {"_id": player.id},
                    {
                        "$set": {
                            "battletag": p,
                            "w3c_profile": f"https://www.w3champions.com/player/{parse_profile_link(p)}",
                        }
                    },
                )


client = pymongo.MongoClient(os.getenv("MONGO_URI"))
db = client[os.getenv("MONGO_DB")]
col = db[os.getenv("MONGO_COL")]


async def main():
    async with aiohttp.ClientSession() as session:
        update_tasks = []
        update_tasks.append(get_w3c_list(session=session))

        players_list = await asyncio.gather(*update_tasks, return_exceptions=True)

        update_names(players_list[0])


def start():
    if os.name == "nt":
        # for windows
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(main())
