from flask import Flask, render_template, request, flash, redirect
import pymongo
from bson.objectid import ObjectId
import pandas as pd
import requests
import csv
import os

from dotenv import load_dotenv

load_dotenv()


app = Flask(__name__)
app.config.update(TEMPLATES_AUTO_RELOAD=True)
app.config.update(SECRET_KEY=os.getenv("SECRET_KEY"))
app.config["UPLOAD_FOLDER"] = "/var/www/uploads"


@app.route("/")
def main():
    return render_template("index.html", title="FOML Draft Tool")


@app.route("/update_mmr")
def update_mmr():
    import mongo_update

    mongo_update.start()
    flash("Updated MMR and Elo for all players", "update_success")
    return redirect("/")


@app.route("/manage")
def manage():
    import mongo_update

    players = mongo_update.get_players_list()
    return render_template(
        "manage.html", players=players, title="Challonge Seed Tool - Manage Players"
    )


@app.route("/import")
def import_page():
    return render_template("import.html", title="Bulk Import")


@app.route("/bulk_upload", methods=["GET", "POST"])
def bulk_upload():
    if request.method == "POST":
        file = request.files["file"]
        if file.filename == "":
            flash("No selected file")
            return redirect(request.url)

        # read csv file of players and battletags into a list of dicts
        if file:
            if not os.path.exists(app.config["UPLOAD_FOLDER"]):
                print("Upload folder does not exist, creating...")
                os.makedirs(app.config["UPLOAD_FOLDER"])
            filename = "players.csv"
            file.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
            players = []
            imported_players = []
            updated_players = []

            # convert csv to list of dicts using pandas
            import pandas as pd

            df = pd.read_csv("uploads/players.csv")
            player_dicts = df.to_dict("records")
            for item in player_dicts:
                players.append(item)
            print(players)

        # connect to mongo and insert missing players
        client = pymongo.MongoClient(os.getenv("MONGO_URI"))
        db = client[os.getenv("MONGO_DB")]
        col = db[os.getenv("MONGO_COL")]

        for player in players:
            # check if player exists in mongo
            if col.find_one({"battletag_lower": player["battletag"].lower()}):
                print(f"{player['battletag']} already exists")
                # find player in mongo and update it
                filter = {"battletag": player["battletag"]}
                new_values = {
                    "$set": {
                        "username": player["username"],
                        "battletag": player["battletag"],
                        "battletag_lower": player["battletag"].lower(),
                        "race": player["race"],
                        "region": player["region"],
                        "w3c_profile": player["w3c_profile"],
                    }
                }
                col.update_one(filter, new_values)
                print(f"Updated {player['battletag']}")
                updated_players.append(player)

            else:
                print(f"Adding {player['username']}")
                col.insert_one(
                    {
                        "username": player["username"],
                        "battletag": player["battletag"],
                        "battletag_lower": player["battletag"].lower(),
                        "race": player["race"],
                        "region": player["region"],
                        "s13_mmr": 0,
                        "s12_mmr": 0,
                        "w3c_profile": f'https://w3champions.com/player/{player["battletag"].replace("#", "%23")}',
                    }
                )
                imported_players.append(player)

    return render_template(
        "bulk_upload.html", players=imported_players, updated_players=updated_players
    )


@app.route("/fix_names")
def fix_names():
    import sync_names

    sync_names.start()
    flash("Names successfully updated", "update_success")
    return redirect("/manage")


def get_single_mmr(battletag):
    season = int(os.getenv("SEASON"))
    s12_mmr_list = [0]
    s13_mmr_list = [0]
    while season >= 12:
        print(f"Checking season {season} mmr")
        url = (
            os.getenv("W3C_URL")
            + battletag.replace("#", "%23")
            + "/game-mode-stats?gateWay=20&season="
            + str(season)
        )

        try:
            res = requests.get(url)
            data = res.json()
        except Exception as e:
            print(e)
            return {"battletag": battletag, "s13_mmr": 0, "s12_mmr": 0}

        for item in data:
            if item["gameMode"] == 1:
                if season == 12:
                    s12_mmr_list.append(item["mmr"])
                elif season == 13:
                    s13_mmr_list.append(item["mmr"])

        season -= 1

    return {
        "battletag": battletag,
        "s13_mmr": max(s13_mmr_list),
        "s12_mmr": max(s12_mmr_list),
    }


@app.route("/update_single_user", methods=["GET", "POST"])
def update_single_user():
    if request.method == "POST":
        id = request.form["id"]
        battletag = request.form["battletag"]

        mmr_dict = get_single_mmr(battletag)
        s12_mmr = mmr_dict["s12_mmr"]
        s13_mmr = mmr_dict["s13_mmr"]

        client = pymongo.MongoClient(os.getenv("MONGO_URI"))
        db = client[os.getenv("MONGO_DB")]
        col = db[os.getenv("MONGO_COL")]

        print(f" Updating {id}- {battletag}")
        col.update_one(
            {"_id": ObjectId(str(id))},
            {
                "$set": {
                    "battletag": battletag,
                    "battletag_lower": battletag.lower(),
                    "s12_mmr": s12_mmr,
                    "s13_mmr": s13_mmr,
                    "w3c_profile": f'https://w3champions.com/player/{battletag.replace("#", "%23")}',
                }
            },
        )
        print(
            f"Updated - id: {ObjectId(str(id))} \
                name: battletag: {battletag} - \
                s12_mmr: {s12_mmr} - \
                s13_mmr: {s13_mmr}"
        )

        flash(
            f"Updated - name: battletag: {battletag} - \
                s12_mmr: {s12_mmr} - \
                s13_mmr: {s13_mmr}",
            "update_success",
        )

    return redirect("/manage")


@app.route("/delete_single_user")
def delete_single_user():
    id = request.args.get("id")
    client = pymongo.MongoClient(os.getenv("MONGO_URI"))
    db = client[os.getenv("MONGO_DB")]
    col = db[os.getenv("MONGO_COL")]

    # pull player name from mongo by id
    player = col.find_one({"_id": ObjectId(str(id))})
    username = player["username"]

    col.delete_one({"_id": ObjectId(str(id))})
    flash(f"{username} successfully deleted", "delete_success")
    return redirect("/manage")


@app.route("/single_insert", methods=["GET", "POST"])
def single_insert():
    title = "Add Player"
    if request.method == "POST":
        username = request.form["username"]
        battletag = request.form["battletag"]
        race = request.form["race"]
        region = request.form["region"]
        w3c_profile = f'https://w3champions.com/player/{battletag.replace("#", "%23")}'

        mmr = get_single_mmr(battletag)

        client = pymongo.MongoClient(os.getenv("MONGO_URI"))
        db = client[os.getenv("MONGO_DB")]
        col = db[os.getenv("MONGO_COL")]

        # check if player exists in mongo
        if col.find_one({"battletag_lower": battletag.lower()}):  # if player exists
            print(f"{battletag} already exists")
            # find player in mongo and update it
            filter = {"battletag": battletag}
            new_values = {
                "$set": {
                    "username": username,
                    "battletag": battletag,
                    "battletag_lower": battletag.lower(),
                    "w3c_profile": w3c_profile,
                    "s12_mmr": mmr["s12_mmr"],
                    "s13_mmr": mmr["s13_mmr"],
                    "race": race,
                    "region": region,
                }
            }
            col.update_one(filter, new_values)
            print(f"Updated {battletag}")
            flash(
                f"Updated - name: {battletag} - \
                    battletag: {battletag}",
                "insert_success",
            )
        else:
            col.insert_one(
                {
                    "username": username,
                    "battletag": battletag,
                    "battletag_lower": battletag.lower(),
                    "w3c_profile": w3c_profile,
                    "s12_mmr": mmr["s12_mmr"],
                    "s13_mmr": mmr["s13_mmr"],
                    "race": race,
                    "region": region,
                }
            )
            print(f"Added {battletag}")
            flash(
                f"Added - name: {username} - \
                    battletag: {battletag}",
                "insert_success",
            )

    return render_template("/single_insert.html", title=title)


@app.route("/export_players")
def export_players():
    """
    This function exports all players from mongo to csv
    """
    client = pymongo.MongoClient(os.getenv("MONGO_URI"))
    db = client[os.getenv("MONGO_DB")]
    col = db[os.getenv("MONGO_COL")]

    # pull all players from mongo
    players = col.find({})

    # convert to list
    player_list = []
    for player in players:
        player_list.append(player)

    # convert to csv
    import pandas as pd

    df = pd.DataFrame(player_list)
    # drop _id column
    df = df.drop(columns=["_id"])
    df.to_csv("static/players.csv", index=False)

    return redirect("/static/players.csv")


@app.route("/delete_players")
def delete_players():
    """
    This function deletes all players from mongo
    """
    client = pymongo.MongoClient(os.getenv("MONGO_URI"))
    db = client[os.getenv("MONGO_DB")]
    col = db[os.getenv("MONGO_COL")]

    col.delete_many({})
    flash("All players successfully deleted", "delete_success")
    return redirect("/manage")
