class Player:
    def __init__(
        self, id, username, battletag, race, w3c_profile, region, s12_mmr, s13_mmr
    ):
        self.id = id
        self.username = username
        self.battletag = battletag
        self.race = race
        self.w3c_profile = w3c_profile
        self.region = region
        self.s12_mmr = s12_mmr
        self.s13_mmr = s13_mmr

    def to_dict(self):
        return {
            "id": self.id,
            "username": self.username,
            "battletag": self.battletag,
            "race": self.race,
            "w3c_profile": self.w3c_profile,
            "region": self.region,
            "s12_mmr": self.s12_mmr,
            "s13_mmr": self.s13_mmr,
        }
